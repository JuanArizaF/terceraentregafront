// Librerías
const { app, BrowserWindow, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

let ventanaPrincipal;



// Evento "ready" de la aplicación
app.on("ready", () => {
  // Creación de la ventana principa
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });
  // Carga del archivo index.html en la ventana
  ventanaPrincipal.loadURL(
    url.format({
      pathname: path.join(__dirname, "views/login.html"),
      protocol: "file",
      slashes: true,
    })
  );
  

  ipcMain.on("hola", (event, data) => {
    let a = data
    setTimeout(() => {
      ventanaPrincipal.webContents.send("Prueba", a)
    }, 9000)
    console.log("--PrimerPunto--"+data); 
  })
  

});

