document.addEventListener("DOMContentLoaded", () => {
    // Boton buscar
    btn_inicar = document.getElementById("btn_eliminar_buscar_bookmark");
    btn_inicar.addEventListener("click", inciar);
    //nombre = "juanariza"


    async function inciar(evento) {
        let nombreP = await axios.get("http://localhost:3000/actuals/")
        console.log("este es el usuario: " + nombreP.data[0]['nombre']);
        nombre = nombreP.data[0]['nombre']

        let bookmarks = await axios.get("http://localhost:3000/bookmarks/" + nombre)
        let bookmark = bookmarks.data
        console.log(bookmark);

        evento.preventDefault();
        let int_nombre = document.getElementById('b_eliminar').value

        if (int_nombre === "" || int_nombre[0] === " ") {
            console.log("invalido");
            let error2 = document.getElementById('b_eliminar')
            error2.style.backgroundColor = "red"
            window.alert("Ingrese el nombre del Bookmark");
        } else {
            let encontrado = false
            for (let i = 0; i < bookmark.length; i++) {
                if (bookmark[i]["nombre"] === int_nombre) {
                    bookmark.splice(i, 1)
                    console.log(bookmark);
                    window.alert("Bookmark encontrado y eliminado con éxito ");
                    encontrado = true
                    let respuesta = await axios.delete("http://localhost:3000/bookmarks/" + int_nombre);
                }

            }
            if (encontrado == false) {
                window.alert("Usted no está registrad@");
            }

        }
    }

})