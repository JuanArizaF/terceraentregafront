const { ipcRenderer } = require('electron');
document.addEventListener("DOMContentLoaded", () => {

    btn_inicar = document.getElementById("btn_iniciar");
    btn_inicar.addEventListener("click", inciar);

    async function inciar(evento) {
        evento.preventDefault();
        let login = document.getElementById('login').value
        let password = document.getElementById('password').value
        let usuarios = await axios.get("http://localhost:3000/usuarios/all")
        let usuario = usuarios.data
        let existe = false

        if (login === "" || password === "" || login[0] === " " || password[0] === " ") {
            console.log("invalido");
            let error2 = document.getElementById('login')
            error2.style.backgroundColor = "red"
            let error3 = document.getElementById('password')
            error3.style.backgroundColor = "red"
            window.alert("Complete todos los campos");
        } else {
            for (let i = 0; i < usuario.length; i++) {
                //console.log(atob(usuario[i]["password"]));
                if (usuario[i]["username"] === login && atob(usuario[i]["password"]) == password) {
                    let actual = await axios.post('http://localhost:3000/actuals/', {nombre:login})
                    console.log("Entro el usuario: "+actual);
                    console.log("Bienvenido ");
                    window.location.replace("../views/index.html");
                    existe = true
                    var nombreP = usuario[i]["username"]
                    console.log(nombreP);
                    ipcRenderer.send("hola",nombreP)

                }
            }
            if (existe == false) {
                window.alert("Usted no está registrad@");
            }
        }
    }

})
