document.addEventListener("DOMContentLoaded", () => {

    btn_registrar = document.getElementById("btn_registrar");
    btn_registrar.addEventListener("click", crearUsuario);

    async function crearUsuario(evento) {
        evento.preventDefault();
        let username = document.getElementById('username').value
        let password = document.getElementById('password').value
        let name = document.getElementById('nombre').value
        let email = document.getElementById('email').value

        if (username === "" || password === "" || name === "" || email === "" || username[0] === " " || password[0] === " " || name[0] === " " || email[0] === " ") {
            console.log("invalido");
            let error = document.getElementById('username')
            error.style.backgroundColor = "red"
            let error1 = document.getElementById('password')
            error1.style.backgroundColor = "red"
            let error2 = document.getElementById('nombre')
            error2.style.backgroundColor = "red"
            let error3 = document.getElementById('email')
            error3.style.backgroundColor = "red"
            window.alert("Complete todos los campos");

        } else {
            let usuario = {
                username: username,
                password: btoa(password),
                nombre: name,
                email: email,
            }
            console.log(usuario);
            let label = {
                nombre: "NINGUNO",
                creador: username
            }
            let labeln = await axios.post('http://localhost:3000/labels', label)
            let user = await axios.post('http://localhost:3000/usuarios/login', usuario)
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
            window.alert("Cuenta creada con éxito");
            window.location.replace("../views/login.html");
        }

    }

})